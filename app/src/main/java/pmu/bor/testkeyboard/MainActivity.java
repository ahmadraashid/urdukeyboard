package pmu.bor.testkeyboard;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    TextView txtNaam;
    TextView txtWaldiat;
    String name = "";
    String waldiat = "";
    int selectedField = 1;

    Button btnClear;
    Button btnBackSpace;
    Button btnSpace;
    Button btnFind;

    //Alphabets
    Button btnAlif;
    Button btnBa;
    Button btnPa;
    Button btnTa;
    Button btnTta;
    Button btnSay;
    Button btnJeem;
    Button btnChay;
    Button btnHay;
    Button btnKhay;
    Button btnDaal;
    Button btnDdaal;
    Button btnZaal;
    Button btnRay;
    Button btnRray;
    Button btnZay;
    Button btnZzaay;
    Button btnSeen;
    Button btnSheen;
    Button btnSwaad;
    Button btnDwaad;
    Button btnTway;
    Button btnZway;
    Button btnAin;
    Button btnGhen;
    Button btnFay;
    Button btnQaaf;
    Button btnKaaf;
    Button btnGaaf;
    Button btnLaam;
    Button btnMeem;
    Button btnNoon;
    Button btnWow;
    Button btnHhay;
    Button btnChotiYay;
    Button btnBariYay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNaam = (TextView) findViewById(R.id.txtNaam);
        txtWaldiat = (TextView) findViewById(R.id.txtWaldiat);

        //Alphabet Buttons
        btnAlif = (Button) findViewById(R.id.btnAlif);
        btnBa = (Button) findViewById(R.id.btnBa);
        btnPa = (Button) findViewById(R.id.btnPa);
        btnTa = (Button) findViewById(R.id.btnTay);
        btnTta = (Button) findViewById(R.id.btnTtay);
        btnSay = (Button) findViewById(R.id.btnSay);
        btnJeem = (Button) findViewById(R.id.btnJeem);
        btnChay = (Button) findViewById(R.id.btnChay);
        btnHay = (Button) findViewById(R.id.btnHay);
        btnKhay = (Button) findViewById(R.id.btnKhay);
        btnDaal = (Button) findViewById(R.id.btnDaal);
        btnDdaal = (Button) findViewById(R.id.btnDdaal);
        btnZaal = (Button) findViewById(R.id.btnZaal);
        btnRay = (Button) findViewById(R.id.btnRay);
        btnRray = (Button) findViewById(R.id.btnRray);
        btnZay = (Button) findViewById(R.id.btnZay);
        btnZzaay = (Button) findViewById(R.id.btnZzay);
        btnSeen = (Button) findViewById(R.id.btnSeen);
        btnSheen = (Button) findViewById(R.id.btnSheen);
        btnSwaad = (Button) findViewById(R.id.btnSwaad);
        btnDwaad = (Button) findViewById(R.id.btnDwaad);
        btnTway  = (Button) findViewById(R.id.btnTway);
        btnZway = (Button) findViewById(R.id.btnZway);
        btnAin = (Button) findViewById(R.id.btnAin);
        btnGhen = (Button) findViewById(R.id.btnGhen);
        btnFay = (Button) findViewById(R.id.btnFay);
        btnQaaf = (Button) findViewById(R.id.btnQaaf);
        btnKaaf = (Button) findViewById(R.id.btnKaaf);
        btnGaaf = (Button) findViewById(R.id.btnGaaf);
        btnLaam = (Button) findViewById(R.id.btnLaam);
        btnMeem = (Button) findViewById(R.id.btnMeem);
        btnNoon = (Button) findViewById(R.id.btnNoon);
        btnWow = (Button) findViewById(R.id.btnWow);
        btnHhay = (Button) findViewById(R.id.btnHhay);
        btnChotiYay = (Button) findViewById(R.id.btnChootiYay);
        btnBariYay = (Button) findViewById(R.id.btnBariYay);

        //Other buttons
        btnClear = (Button) findViewById(R.id.btnClear);
        btnBackSpace = (Button) findViewById(R.id.btnBackSpace);
        btnSpace = (Button) findViewById(R.id.btnSpace);
        btnFind = (Button) findViewById(R.id.btnFind);

        txtNaam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNaam.setBackgroundColor(getResources().getColor(R.color.lightYellow));
                txtWaldiat.setBackgroundColor(getResources().getColor(R.color.lightGray));
                selectedField = 1;
            }
        });

        txtWaldiat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtWaldiat.setBackgroundColor(getResources().getColor(R.color.lightYellow));
                txtNaam.setBackgroundColor(getResources().getColor(R.color.lightGray));
                selectedField = 2;
            }
        });

        btnFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //showRatingDialog();
                Intent intent = new Intent(getBaseContext(), RatingActivity.class);
                startActivity(intent);
            }
        });

        //Keyboard key events
        btnAlif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.alif));
            }
        });

        btnBa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.bay));
            }
        });

        btnPa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.pa));
            }
        });

        btnTa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.tay));
            }
        });

        btnTta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.ttay));
            }
        });

        btnSay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.say));
            }
        });

        btnJeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.jeem));
            }
        });

        btnChay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.chay));
            }
        });

        btnHay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.hay));
            }
        });

        btnKhay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.khay));
            }
        });

        btnDaal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.daal));
            }
        });

        btnDdaal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.ddaal));
            }
        });

        btnZaal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.zaal));
            }
        });

        btnRay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.ray));
            }
        });

        btnRray.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.rray));
            }
        });

        btnZay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.zay));
            }
        });

        btnZzaay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.zzay));
            }
        });

        btnSeen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.seen));
            }
        });

        btnSheen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.sheen));
            }
        });

        btnSwaad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.swaad));
            }
        });

        btnDwaad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.dwaad));
            }
        });

        btnTway.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.tway));
            }
        });

        btnZway.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.zway));
            }
        });

        btnAin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.ain));
            }
        });

        btnGhen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.ghen));
            }
        });

        btnFay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.fay));
            }
        });

        btnQaaf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.qaaf));
            }
        });

        btnKaaf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.kaaf));
            }
        });

        btnGaaf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.gaaf));
            }
        });

        btnLaam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.laam));
            }
        });

        btnMeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.meem));
            }
        });

        btnNoon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.noon));
            }
        });

        btnWow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.wow));
            }
        });

        btnHhay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.hhay));
            }
        });

        btnChotiYay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.chootiYay));
            }
        });

        btnBariYay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.bariYay));
            }
        });

        btnSpace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageText(getResources().getString(R.string.space));
            }
        });

        //Other buttons
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedField == 1) {
                    txtNaam.setText("");
                } else {
                    txtWaldiat.setText("");
                }
            }
        });

        btnBackSpace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedField == 1) {
                    String name = txtNaam.getText().toString();
                    name = (name == null || name.length() == 0) ? null : (name.substring(0, name.length() - 1));
                    txtNaam.setText(name);
                } else {
                    String parent = txtWaldiat.getText().toString();
                    parent = (parent == null || parent.length() == 0) ? null : (parent.substring(0, parent.length() - 1));
                    txtWaldiat.setText(parent);
                }
            }
        });


    }

    private void manageText(String str) {
        if (str.length() == 0)
            str = " ";
        if (selectedField == 1) {
            txtNaam.setText(txtNaam.getText() + str);
        } else {
            txtWaldiat.setText(txtWaldiat.getText() + str);
        }
    }

    public class SetKeyBoardAsyncTasks extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            return "Ok";
        }

        @Override
        protected void onPostExecute(String s) {

        }
    }

    public void showRatingDialog()
    {
        final AlertDialog.Builder popDialog = new AlertDialog.Builder(this);

        final RatingBar rating = new RatingBar(this);
        rating.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        rating.setPadding(25,5,25,5);

        rating.setRating((float)0.0);
        rating.setNumStars(5);
        rating.setMax(5);
        rating.setStepSize((float)1.0);

        //LayoutInflater inflater = getActivity().getLayoutInflater();


        popDialog.setIcon(android.R.drawable.btn_star_big_on);
        popDialog.setTitle("Vote!! ");
        popDialog.setView(rating);
        // Button OK
        popDialog.setPositiveButton(android.R.string.ok,
        new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                txtNaam.setText(String.valueOf(rating.getProgress()));
                dialog.dismiss();
            }
        })
                .setNegativeButton("Cancel",
        new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        popDialog.create();
        popDialog.show();
    }

}
